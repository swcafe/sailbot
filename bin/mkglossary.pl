while(<>) {
    if (/^<dt.*<dfn>(.*)<\/dfn>/) {
    	print "'$word': '$defn',\n" if $word and $defn;
	$word = lc $1;
	$word =~ s/<.*?>//g;
	$word =~ s/[^a-z ]//g;
	$defn = '';
    }
    if (/^<dd>(.*)<\/dd>/) {
        $def = $1;
	$def =~ s/'/\\'/g;
    	if ($defn) {
	    $defn .= ' ' . $def;
	} else {
	    $defn = $def;
	}
    }
}
__END__

<dt id="Abaft" style="margin-top: 0.4em;"><dfn>Abaft</dfn></dt>
<dd>Toward the stern, relative to some object ("abaft the fore hatch").</dd>
<dt id="Abaft_the_beam" style="margin-top: 0.4em;"><dfn>Abaft the beam</dfn></dt>
<dd>Further aft than the beam: a relative bearing of greater than 90 degrees from the bow: "two points abaft the beam, starboard side". That would describe "an object lying 22.5 degrees toward the rear of the ship, as measured clockwise from a perpendicular line from the right side, center, of the ship, toward the horizon."<sup id="cite_ref-navalslang_2-0" class="reference"><a href="#cite_note-navalslang-2"><span>[</span>2<span>]</span></a></sup></dd>
<dt id="Abandon_ship.21" style="margin-top: 0.4em;"><dfn>Abandon ship!</dfn></dt>
<dd>An imperative to leave the vessel immediately, usually in the face of some imminent overwhelming danger.<sup id="cite_ref-Hope_3-0" class="reference"><a href="#cite_note-Hope-3"><span>[</span>3<span>]</span></a></sup> It is an order issued by the Master or a delegated person in command. It is usually the last resort after all other mitigating actions have failed or become impossible, and destruction or loss of the ship is imminent; and customarily followed by a command to "man the lifeboats" or life rafts.<sup id="cite_ref-Hope_3-1" class="reference"><a href="#cite_note-Hope-3"><span>[</span>3<span>]</span></a></sup><sup id="cite_ref-Layton_4-0" class="reference"><a href="#cite_note-Layton-4"><span>[</span>4<span>]</span></a></sup></dd>
<dt id="Abeam" style="margin-top: 0.4em;"><dfn>Abeam</dfn></dt>
<dd><i>On the beam</i>, a relative bearing at right angles to the centerline of the ship's <a href="/wiki/Keel" title="Keel">keel</a>.</dd>
<dt style="margin-top: 0.4em;"><dfn>"<a href="/wiki/Barnacle_Bill_(song)" title="Barnacle Bill (song)">Abel Brown</a>"</dfn></dt>
<dd>A <a href="/wiki/Sea_shanty" title="Sea shanty">sea shanty</a> (song) about a young sailor trying to sleep with a maiden<sup id="cite_ref-5" class="reference"><a href="#cite_note-5"><span>[</span>5<span>]</span></a></sup></dd>
